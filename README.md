### Создание конфигурационных файлов
```
cp config/database.yml.example config/database.yml
cp config/secrets.yml.example config/secrets.yml
```

### Инициализация базы данных
`bundle exec db:setup`

### Соглашения по стилю кода

Перед коммитом необходимо убедиться, что ваш код соответствует принятому стилю оформления.

`bundle exec rubocop`
