json.extract! message, :id, :body, :created_at
json.user do
  json.extract! message.user, :id, :name
end
