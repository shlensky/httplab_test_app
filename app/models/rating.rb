# == Schema Information
#
# Table name: ratings
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  message_id :integer          not null
#  value      :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Rating < ActiveRecord::Base
  belongs_to :user, inverse_of: :ratings
  belongs_to :message, inverse_of: :ratings

  validates :user, presence: true
  validates :message, presence: true
  validates :message_id, uniqueness: { scope: [:user_id] }
  validates :value, presence: true,
                    numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }
end
