# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Message < ActiveRecord::Base
  belongs_to :user, inverse_of: :messages
  has_many :ratings, inverse_of: :message, dependent: :destroy

  validates :body, presence: true, length: { maximum: 100.kilobytes }
  validates :user, presence: true
end
