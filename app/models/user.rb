# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  login           :string           not null
#  password_digest :string           not null
#  token           :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ActiveRecord::Base
  has_secure_password
  has_secure_token

  has_many :messages, inverse_of: :user, dependent: :destroy
  has_many :ratings, inverse_of: :user, dependent: :destroy

  validates :name, presence: true, length: { maximum: 255 }
  validates :login, presence: true, uniqueness: true, length: { maximum: 64 }
  validates :password, length: { in: 6..20 }, allow_nil: true
end
