module Api
  class MessagesController < Api::BaseApiController
    before_action :authenticate_with_token!

    # TODO: pagination
    # TODO: return ratings
    def index
      @messages = Message.order(created_at: :desc).includes(:user)
    end

    def create
      message = current_user.messages.build(message_params)
      if message.save
        render json: message, status: 201
      else
        render json: { errors: message.errors }, status: 422
      end
    end

    private

    def message_params
      params.permit(:body)
    end
  end
end
