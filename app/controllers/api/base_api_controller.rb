module Api
  class BaseApiController < ApplicationController
    protect_from_forgery with: :null_session

    def current_user
      @current_user ||= User.find_by(id: request.headers['User-Id'], token: request.headers['Token'])
    end

    def authenticate_with_token!
      render json: { errors: 'Not authenticated' }, status: :unauthorized unless current_user.present?
    end
  end
end
