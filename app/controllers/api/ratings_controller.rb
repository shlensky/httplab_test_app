module Api
  class RatingsController < Api::BaseApiController
    before_action :authenticate_with_token!
    before_action :set_message

    def create
      rating = @message.ratings.where(user_id: current_user.id).first_or_initialize
      if rating.update(rating_params)
        render json: rating, status: 201
      else
        render json: { errors: rating.errors }, status: 422
      end
    end

    private

    def set_message
      @message = Message.find(params[:message_id])
    end

    def rating_params
      params.permit(:value)
    end
  end
end
