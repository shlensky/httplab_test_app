module Api
  class SessionsController < Api::BaseApiController
    def create
      user = User.find_by(login: params[:login])

      if user.try(:authenticate, params[:password])
        user.regenerate_token

        render json: user, status: 200
      else
        render json: { errors: 'Invalid login or password' }, status: 422
      end
    end

    def destroy
      user = User.find_by(token: params[:id])
      if user.present?
        user.regenerate_token
        head 204
      else
        render json: { errors: 'User not found' }, status: 404
      end
    end
  end
end
