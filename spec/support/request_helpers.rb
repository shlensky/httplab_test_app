module RequestHelpers
  def json_response
    @json_response ||= JSON.parse(response.body, symbolize_names: true)
  end

  def api_authorization_headers(user)
    request.headers['User-Id'] = user.id
    request.headers['Token'] = user.token
  end

  def include_default_accept_headers
    request.headers['Accept'] = Mime::JSON
  end
end
