require 'rails_helper'

describe Api::RatingsController do
  render_views

  describe 'POST #create' do
    let(:user) { create(:user) }
    let(:message) { create(:message) }

    context 'when is successfully created' do
      before(:each) do
        api_authorization_headers user
        post :create, message_id: message.id, value: 5
      end

      it 'renders the json representation for the rating' do
        expect(json_response[:value]).to eql 5
      end

      it { is_expected.to respond_with 201 }
    end

    context 'when is not created' do
      before(:each) do
        api_authorization_headers user
        post :create, message_id: message.id, value: 6
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on why the rating could not be created' do
        expect(json_response[:errors][:value]).to include 'must be less than or equal to 5'
      end

      it { is_expected.to respond_with 422 }
    end

    context 'without authorization' do
      let(:user) { build(:user) }

      before(:each) do
        api_authorization_headers user
        post :create, message_id: message.id, value: 5
      end

      it { is_expected.to respond_with 401 }
    end
  end
end
