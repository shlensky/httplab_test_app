require 'rails_helper'

describe Api::MessagesController do
  render_views

  describe 'GET #index' do
    let(:user) { create(:user) }
    let!(:messages) { create_list(:message, 3) }

    before(:each) do
      api_authorization_headers user
      get :index
    end

    it 'returns records ordered by created_at desc' do
      ids = Message.order(created_at: :desc).pluck(:id)

      expect(json_response[0][:id]).to eql ids[0]
      expect(json_response[1][:id]).to eql ids[1]
      expect(json_response[2][:id]).to eql ids[2]
    end

    it 'returns information about message author' do
      message = Message.order(created_at: :desc).first

      expect(json_response[0][:user][:name]).to eql message.user.name
    end

    it { should respond_with 200 }
  end

  describe 'POST #create' do
    context 'when is successfully created' do
      let(:user) { create(:user) }

      before(:each) do
        api_authorization_headers user
        post :create, user_id: user.id, body: 'Lorem ipsum'
      end

      it 'renders the json representation for the message' do
        expect(json_response[:body]).to eql 'Lorem ipsum'
      end

      it { is_expected.to respond_with 201 }
    end

    context 'when is not created' do
      let(:user) { create(:user) }

      before(:each) do
        api_authorization_headers user
        post :create, user_id: user.id, body: ''
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on why the message could not be created' do
        expect(json_response[:errors][:body]).to include 'can\'t be blank'
      end

      it { is_expected.to respond_with 422 }
    end

    context 'without authorization' do
      let(:user) { build(:user) }

      before(:each) do
        api_authorization_headers user
        post :create, user_id: user.id, body: 'Lorem ipsum'
      end

      it { is_expected.to respond_with 401 }
    end
  end
end
