require 'rails_helper'

describe Api::SessionsController do
  let(:user) { create :user, password: '12345678' }

  describe 'POST #create' do
    context 'when the credentials are correct' do
      before(:each) { post :create, login: user.login, password: user.password }

      it { is_expected.to respond_with 200 }

      it 'returns the user record corresponding to the given credentials' do
        expect(json_response[:id]).to eql user.id
        expect(json_response[:login]).to eql user.login
      end

      it 'regenerates token after sign in' do
        expect(json_response[:token]).to_not eql user.token
        user.reload
        expect(json_response[:token]).to eql user.token
      end
    end

    context 'when the credentials are incorrect' do
      before(:each) { post :create, login: user.login, password: 'invalid' }

      it { is_expected.to respond_with 422 }
      it 'returns a json with an error' do
        expect(json_response[:errors]).to eql 'Invalid login or password'
      end
    end

    context 'when user not exists' do
      before(:each) { post :create, login: 'invalid', password: user.password }

      it { is_expected.to respond_with 422 }
      it 'returns a json with an error' do
        expect(json_response[:errors]).to eql 'Invalid login or password'
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'with valid token' do
      before(:each) { delete :destroy, id: user.token }

      it { is_expected.to respond_with 204 }
      it 'regenerates token after sign out' do
        old_token = user.token
        user.reload
        expect(user.token).to_not eql old_token
      end
    end

    context 'with invalid token' do
      before(:each) { delete :destroy, id: 'invalid' }

      it { is_expected.to respond_with 404 }
      it 'returns a json with an error' do
        expect(json_response[:errors]).to eql 'User not found'
      end
    end
  end
end
