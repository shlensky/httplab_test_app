# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

describe Message do
  # Factories
  let(:message) { create(:message) }
  it { expect(message).to be_valid }

  # Relationships
  it { is_expected.to belong_to(:user).inverse_of(:messages) }

  # Validations
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_length_of(:body).is_at_most(100.kilobytes) }

    it { is_expected.to validate_presence_of(:user) }
  end
end
