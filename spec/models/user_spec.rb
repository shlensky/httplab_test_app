# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  login           :string           not null
#  password_digest :string           not null
#  token           :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

describe User do
  it { should have_secure_password }

  # Factories
  let(:user) { create(:user) }
  it { expect(user).to be_valid }

  # Relationships
  it { is_expected.to have_many(:messages).inverse_of(:user).dependent(:destroy) }

  # Validations
  describe 'Validations' do
    before { create(:user) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(255) }

    it { is_expected.to validate_presence_of(:login) }
    it { is_expected.to validate_uniqueness_of(:login) }
    it { is_expected.to validate_length_of(:login).is_at_most(64) }

    it { is_expected.to validate_length_of(:password).is_at_least(6).is_at_most(20) }
  end
end
