# == Schema Information
#
# Table name: ratings
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  message_id :integer          not null
#  value      :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

describe Rating do
  # Relationships
  it { is_expected.to belong_to(:user).inverse_of(:ratings) }
  it { is_expected.to belong_to(:message).inverse_of(:ratings) }

  # Validations
  describe 'Validations' do
    before { create(:rating) }

    it { is_expected.to validate_presence_of(:user) }

    it { is_expected.to validate_presence_of(:message) }
    it { is_expected.to validate_uniqueness_of(:message_id).scoped_to(:user_id) }

    it { is_expected.to validate_presence_of(:value) }
    it do
      is_expected.to validate_numericality_of(:value).only_integer
        .is_greater_than_or_equal_to(1).is_less_than_or_equal_to(5)
    end
  end
end
