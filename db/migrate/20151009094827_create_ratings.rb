class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.belongs_to :user, index: true, foreign_key: true, null: false
      t.belongs_to :message, index: true, foreign_key: true, null: false
      t.integer :value, null: false

      t.timestamps null: false
    end
  end
end
