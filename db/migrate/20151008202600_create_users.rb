class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :login, null: false
      t.string :password_digest, null: false
      t.string :token, null: false

      t.timestamps null: false
    end

    add_index :users, :login, unique: true
    add_index :users, :token, unique: true
  end
end
